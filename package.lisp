
(uiop:define-package #:wim
  (:use :cl)
  (:import-from #:trivial-logs
                #:with-logging)
  (:import-from #:alexandria
                #:when-let
                #:if-let))
