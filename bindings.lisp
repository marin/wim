
(in-package :wim)


;;; Classes

(defclass binding ()
  ((state :initarg :state :accessor binding-state)
   (binding :initarg :binding :accessor binding-binding)))

(defclass key-binding (binding)
  ((keycode :initarg :keycode :accessor binding-keycode)))

(defclass button-binding (binding)
  ((button :initarg :button :accessor binding-button)
   (event-mask :initarg :event-mask :accessor binding-event-mask)))

(defclass keymap ()
  ((bindings :initarg :bindings :initform nil :accessor keymap-bindings)))


;;; Special Variables
(defvar *top-level-map* (make-instance 'keymap :bindings nil)
  "The top level binding map")

;; Dont modify this directly, only through invoke-with-grabbed-keymap function
(defvar *current-keymaps* ()
  "A list of all currently bound keymaps")

(defvar *load-keymaps-transparently* nil
  "When T, keymaps loaded by load-keymap are loaded transparently.")


;;; Keymap Loading
(defun invoke-with-grabbed-keymap (continuation keymap &key (transparently t))
  "Bind KEYMAP bindings and then call CONTINUATION. If TRANSPARENTLY is T then
this keymap is bound transparently over the previously established keymap,
meaning that the prior keymaps bindings are still grabbed from the X server. 

If REPLACE-KEYMAP is provided it is assumed to be a keymap, and is unbound and
then bound."
  (let ((*current-keymaps* (cons keymap *current-keymaps*))
        (maps *current-keymaps*))
    (flet ((transparent (fn)
             (map nil (lambda (map) (map nil fn (keymap-bindings map))) maps)))
      (unless transparently
        (transparent 'ungrab-binding))
      (map nil 'grab-binding (keymap-bindings keymap))
      (unwind-protect (funcall continuation)
        (mapcar 'ungrab-binding (keymap-bindings keymap))
        (unless transparently
          (transparent 'grab-binding))))))

(defmacro with-keymap ((keymap &key transparently) &body body)
  `(flet-def-and-call (()
                       invoke-with-grabbed-keymap
                       (,keymap :transparently ,transparently))
     ,@body))

(defun load-keymap (keymap &optional (transparently *load-keymaps-transparently*))
  "Load MAP and continue processing"
  (declare (type keymap keymap))
  (with-keymap (keymap :transparently transparently)
    (do-processing ())))


;;; Protocol Implementation

(defmethod binding-equal-p ((binding-a key-binding) (binding-b key-binding))
  (and (= (binding-keycode binding-a) (binding-keycode binding-b))
       (alexandria:set-equal (binding-state binding-a) (binding-state binding-b))))

(defmethod binding-equal-p ((binding-a button-binding) (binding-b button-binding))
  (and (= (binding-button binding-a) (binding-button binding-b))
       (alexandria:set-equal (binding-state binding-a) (binding-state binding-b))))

(defmethod find-binding (key state (map keymap))
  (find-binding key state (keymap-bindings map)))

(defmethod find-binding ((keycode number) state (map list))
  (let ((temporary-binding (make-instance 'key-binding :keycode keycode
                                                       :state state)))
    (declare (dynamic-extent temporary-binding))
    (find temporary-binding map :test #'binding-equal-p)))

(defmethod find-binding ((button cons) state (map list))
  (let ((temporary-binding (make-instance 'button-binding :button (car button)
                                                          :state state)))
    (declare (dynamic-extent temporary-binding))
    (find temporary-binding map :test #'binding-equal-p)))

(defmethod bind :before (map bound-to state binding)
  (declare (ignore binding))
  (unbind map bound-to state))

(defmethod bind ((map keymap) (keycode number) state binding)
  (push (make-instance 'key-binding :keycode keycode
                                    :state state
                                    :binding binding)
        (keymap-bindings map)))

(defmethod bind ((map keymap) (button cons) state binding)
  (push (make-instance 'button-binding :button (car button)
                                       :state state
                                       :binding binding
                                       :event-mask (cdr button))
        (keymap-bindings map)))

(defmethod unbind ((map keymap) (keycode number) state)
  (setf (keymap-bindings map)
        (remove-if (lambda (bind)
                     (and (typep bind 'key-binding)
                          (equal (binding-keycode bind) keycode)
                          (equal (binding-state bind) state)))
                   (keymap-bindings map))))

(defmethod unbind ((map keymap) (button cons) state)
  (setf (keymap-bindings map)
        (remove-if (lambda (bind)
                     (and (typep bind 'button-binding)
                          (equal (binding-button bind) (car button))
                          (equal (binding-state bind) state)))
                   (keymap-bindings map))))

(defmethod grab-binding ((binding key-binding))
  (xlib:grab-key *root-window*
                 (binding-keycode binding)
                 :modifiers (binding-state binding)))

(defmethod grab-binding ((binding button-binding))
  (xlib:grab-button *root-window*
                    (binding-button binding)
                    (binding-event-mask binding)
                    :modifiers (binding-state binding)))

(defmethod ungrab-binding ((binding key-binding))
  (xlib:ungrab-key *root-window*
                   (binding-keycode binding)
                   :modifiers (binding-state binding)))

(defmethod ungrab-binding ((binding button-binding))
  (xlib:ungrab-button *root-window*
                      (binding-button binding)
                      :modifiers (binding-state binding)))

(defmethod %invoke-binding ((binding function))
  (funcall binding))

(defmethod %invoke-binding ((binding symbol))
  (load-keymap (symbol-value binding)))

(defmethod %invoke-binding ((binding keymap))
  (load-keymap binding))

(defmethod %invoke-binding ((binding list))
  (apply (car binding) (cdr binding)))

(defmethod %invoke-binding ((binding string))
  (eval (read-from-string binding)))

(defmethod %invoke-binding (binding)
  (trivial-logs:log "Unable to invoke binding ~A" binding))

(defmethod invoke-binding ((binding binding))
  (%invoke-binding (binding-binding binding)))

(defmethod invoke-binding (binding)
  (trivial-logs:log "Unknown binding ~A" binding))


;;; Helpers and Wrappers

(defmacro defbind ((map key (&rest mods) &key (exit-processing-level 0))
                   &body body)
  "Bind something to a key in a map. If the initial element of BODY is the
keyword :keymap then the second element of BODY is assumed to be a form which
evaluates to a keymap or a symbol denoting a keymap. Otherwise a lambda is
generated containing BODY. If EXIT-PROCESSING-LEVEL is non-nil then BODY is
wrapped in an unwind-protect form which exits EXIT-PROCESSING-LEVEL levels of
processing. By default it exits to the toplevel"
  `(bind ,map ',key ,(when mods `(list ,@mods))
         ,(if (and (atom (car body))
                   (eql (car body) :keymap))
              `,(cadr body)
              `(lambda ()
                 ,@(if exit-processing-level 
                       `((unwind-protect (progn ,@body)
                           (up-processing-level ,exit-processing-level)))
                       body)))))
