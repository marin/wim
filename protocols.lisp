
(in-package :wim)


;;; Binding Protocol

(defgeneric binding-equal-p (binding-a binding-b)
  (:documentation "Check if two bindings are equivalent")
  (:method (ba bb) nil))

(defgeneric find-binding (bound-to state map))

(defgeneric bind (map bound-to state binding)
  (:documentation
   "Low level binding facility. Bind a key or button, with its accompanying state,
   to BINDING in the keymap MAP. the key or button is represented in numerical
   terms. "))

(defgeneric unbind (map bind state))

(defgeneric grab-binding (binding))

(defgeneric ungrab-binding (binding))

(defgeneric %invoke-binding (binding))

(defgeneric invoke-binding (binding))


;;; Logging Protocol

(defgeneric log-event (key event-slots &optional control-string &rest arguments))
