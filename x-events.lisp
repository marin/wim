
(in-package :wim)

(defparameter *display* nil
  "The X11 display.")
(defparameter *root-window* nil
  "The root window.")

(defvar *event-handlers* (make-hash-table)
  "A hash table of X11 event keys and their handler functions.")

(defvar *event-key-list* ()
  "A list of X11 event keys for a given event.")

(defvar *return-to-previous-event-processing-level* nil
  "A list of thunks which return directly to a specific processing level.")


;;; Process Events

(defun process-events (return-thunk &optional (display *display*))
  (let ((*return-to-previous-event-processing-level*
          (cons return-thunk *return-to-previous-event-processing-level*)))
    (loop (xlib:process-event display :handler #'handle-xlib-event
                                      ;; :timeout 0
                                      :discard-p t))))

(defun up-processing-level (&optional (levels 1) &aux (elt (- levels 1)))
  (when-let ((thunk
              (handler-case (elt *return-to-previous-event-processing-level* elt)
                (error () ; elt to big (or sub 0) then pop to top level
                  (car (last *return-to-previous-event-processing-level*))))))
    (xlib:display-finish-output *display*)
    (funcall thunk)))

(defmacro do-processing ((&optional (display '*display*)) &body handlers)
  (with-gensyms (old-keys disp block)
    `(block ,block
       (let ((,old-keys (mapcar (lambda (key)
                                  (cons key (gethash key *event-handlers*)))
                                ',(mapcar #'car handlers)))
             (,disp ,display))
         (xlib:display-finish-output ,disp)
         (unwind-protect
              (progn ,@(mapcar (lambda (handler)
                                 `(setf (gethash ,(car handler) *event-handlers*)
                                        ,(cadr handler)))
                               handlers)
                     (process-events (lambda () (return-from ,block nil))
                                     ,disp))
           (mapcar (lambda (hasher)
                     (setf (gethash (car hasher) *event-handlers*) (cdr hasher)))
                   ,old-keys))))))


;;; Event Handlers

(defun handle-xlib-event (&rest slots &key display event-key &allow-other-keys)
  (declare (ignore display))
  (let ((*event-key-list* slots))
    ;; (trivial-logs:log "handling ~S" event-key)
    ;; (log-event event-key slots)
    (if-let ((handler (gethash event-key *event-handlers*)))
      (apply handler slots)
      (log-event event-key slots "No handler found for ~S" event-key)
      ;; (trivial-logs:log "No handler found for ~S" event-key)
      )))

(defmacro make-handler (arguments &body body)
  `(lambda ,(if (atom arguments)
                `(&rest ,arguments)
                `(&key ,@arguments &allow-other-keys))
     ,@body))

(defmacro defhandler (event-key arguments &body body)
  `(setf (gethash ,event-key *event-handlers*)
         (make-handler ,arguments ,@body)))

(defhandler :key-release (code state)
  (log-event :key-release *event-key-list* "key-release (~S, ~S)" code state)
  ;; (trivial-logs:log "key-release (~S, ~S)" code state)
  )

(defhandler :key-press (code state)
  (log-event :key-press *event-key-list* "key-press (~S, ~S)" code state)
  (invoke-binding (apply #'find-in-lists-if
                         (lambda (map)
                           (find-binding code
                                         (xlib:make-state-keys state)
                                         map))
                         *current-keymaps*)))

(defhandler :button-press (code state)
  (log-event :button-press *event-key-list* "button-press (~S, ~S)" code state)
  (invoke-binding (apply #'find-in-lists-if
                         (lambda (map)
                           (find-binding (list code)
                                         (xlib:make-state-keys state)
                                         map))
                         *current-keymaps*)))

(defhandler :mapping-notify (start count request)
  (xlib:mapping-notify *display* request start count))

(defhandler :configure-request (stack-mode window))

(defhandler :map-request (parent send-event-p window)
  (case (xlib:window-map-state window)
    ((:unmapped)
     (xlib:map-window window))))
