
(in-package :wim)

(define-condition log-event-signal (trivial-logs:log-signal) ())

(defun logev (control-string &rest args)
  (trivial-logs:write-log (apply #'format nil control-string args)
                          :signal 'log-event-signal))

(macrolet ((evlog (str &rest args)
             (with-gensyms (s)
               `(let ((,s ,str))
                  (if control-string
                      (logev (concatenate 'string ,s "~%~A") ,@args user-format)
                      (logev ,s ,@args))))))
  (symbol-macrolet ((user-format (apply #'format nil control-string arguments)))
    (defmethod log-event (event-key slots &optional control-string &rest arguments)
      (evlog "Unknown Event Key: ~S~%Slots: ~S" event-key slots))

    (defmethod log-event ((event-key (eql :key-release)) slots
                          &optional control-string &rest arguments)
      (with-keys (code state) slots
        (evlog "Key Release (~S ~S)" code state)))

    (defmethod log-event ((event-key (eql :key-press)) slots
                          &optional control-string &rest arguments)
      (with-keys (code state) slots
        (evlog "Key Press (~S ~S)" code state)))))
