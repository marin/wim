
(in-package :wim)

(defvar *minimum-window-size* 250
  "The minimum size a window can shrink to")

(defun put-window-on-top (window)
  (setf (xlib:window-priority window) :above))
