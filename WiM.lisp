
(in-package :wim)

(defun user-directory ()
  (UIOP:GETENV "HOME"))

(defun wimdir ()
  (concatenate 'string (user-directory) "/.wim.d"))

(defvar *wim-event-log*
  (make-pathname :directory (wimdir)
                 :name "x11-events"
                 :type "log"
                 :version :newest)
  ;; "~/.wim.d/x11-events.log"
  "The file to log events to")
(defvar *wim-log*
  (make-pathname :directory (wimdir)
                 :name "wim"
                 :type "log"
                 :version :newest)
  ;; "~/.wim.d/wim.log"
  "The general log file")

(defbind (*top-level-map* (1 :button-press :button-release) (:mod-1))
  (with-keys (child) *event-key-list*
    (log-event :button-press *event-key-list*)
    (put-window-on-top child)
    (with-pointer-restoration (child)
      (xlib:warp-pointer *root-window*
                         (xlib:drawable-x child)
                         (xlib:drawable-y child))
      (with-grabbed-pointer (child '(:pointer-motion :button-release))
        (do-processing ()
          (:button-press
           (make-handler (code state)
             (log-event :button-press *event-key-list*
                        "attempted button press event (~S ~S) when moving window"
                        code state)))
          (:button-release
           (make-handler (code)
             (log-event :button-release *event-key-list*
                        "Button release code ~A" code)
             (when (= code 1)
               (up-processing-level))))
          (:motion-notify
           (make-handler (root-x root-y)
             (log-event :motion-notify *event-key-list*
                        "Motion notify event (~A, ~A)" root-x root-y)
             (setf (xlib:drawable-x child) root-x
                   (xlib:drawable-y child) root-y))))))))

(defbind (*top-level-map* (3 :button-press :button-release) (:mod-1))
  (with-keys (child) *event-key-list*
    (put-window-on-top child)
    (with-pointer-restoration (child)
      (xlib:warp-pointer child
                         (xlib:drawable-width child)
                         (xlib:drawable-height child))
      (with-grabbed-pointer (child '(:pointer-motion :button-release))
        (do-processing ()
          (:button-press nil)
          (:button-release
           (make-handler (code)
             (log-event :button-release *event-key-list*
                        "Button release code ~A" code)
             (when (= code 3)
               (up-processing-level))))
          (:motion-notify
           (make-handler (root-x root-y)
             (setf (xlib:drawable-width child)
                   (max *minimum-window-size*
                        (- root-x (xlib:drawable-x child)))
                   (xlib:drawable-height child)
                   (max *minimum-window-size*
                        (- root-y (xlib:drawable-y child)))))))))))

(defun launch-on-a-whim ()
  (with-logging (*wim-event-log* :dont-close t
                                 :newlines t
                                 :duplicate-to-streams (list *error-output*)
                                 :log-condition log-event-signal
                                 :if-exists :rename)
    (with-logging (*wim-log* :dont-close t
                             :newlines t
                             :duplicate-to-streams (list *error-output*)
                             :if-exists :rename)
      (let* ((*display* (xlib:open-default-display))
             (screen (car (xlib:display-roots *display*)))
             (*root-window* (xlib:screen-root screen)))
        (load "~/.wim.d/init.lisp")
        (unwind-protect
             (with-keymap (*top-level-map* :transparently t)
               (loop (xlib:process-event *display* :handler #'handle-xlib-event
                                         ;; :timeout 0
                                         :discard-p t)))
          (xlib:close-display *display*))))))

