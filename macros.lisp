
(in-package :wim)

(defmacro with-gensyms ((&rest syms) &body body)
  `(let ,(mapcar (lambda (s) (list s `',(gensym (symbol-name s)))) syms)
     ,@body))

(defmacro flet-def-and-call ((args function call-with &key (dynamic-extent t))
                             &body body)
  "Generate a single local function with argument list ARGS using flet and apply
FUNCTION with this function as its first argument and CALL-WITH as its remaining
arguments.

Used for defining with-resource macros."
  (let ((f (gensym "CONT")))
    `(flet ((,f ,args ,@body))
       ,@(when dynamic-extent
           `((declare (dynamic-extent #',f))))
       (,function #',f ,@call-with))))

(defmacro with-keys (keys list &body body)
  "Bind KEYS in LIST as in destructuring-bind"
  `(destructuring-bind (&key ,@keys &allow-other-keys) ,list ,@body))

(defmacro without-keys ((keylist &rest keys) &body body)
  "Bind KEYLIST to a copy of KEYLIST and remove KEYS from it with remf."
  `(let ((,keylist (copy-list ,keylist)))
     ,@(mapcar (lambda (key) `(remf ,keylist ,key)) keys)
     ,@body))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun parse-decls (body &aux docstring decls)
    (macrolet ((grab-when (condition)
                 `(when ,condition
                    (prog1 (car body)
                      (setf body (cdr body))))))
      (setf docstring (grab-when (stringp (car body)))
            decls (grab-when (and (listp (car body))
                                  (eql (caar body) 'declare))))
      (values docstring decls body))))

(defmacro with-parsed-declarations ((docstring declare form-body) form* &body body)
  `(multiple-value-bind (,docstring ,declare ,form-body)
       (parse-decls ,form*)
     ,@body))


;;; Pointer management

(defmacro with-grabbed-pointer ((window event-keys &rest args) &body body)
  `(unwind-protect (progn (xlib:grab-pointer ,window ,event-keys ,@args)
                          ,@body)
     (xlib:ungrab-pointer *display*)))

(defmacro with-saved-pointer-coordinates ((x y &optional (window '*root-window*))
                                          &body body)
  `(multiple-value-bind (,x ,y) (xlib:query-pointer ,window) ,@body))

(defmacro with-pointer-restoration ((&optional (window '*root-window*)) &body body)
  (with-gensyms (x y win)
    `(let ((,win ,window))
       (with-saved-pointer-coordinates (,x ,y ,win)
         (unwind-protect (progn ,@body)
           (xlib:warp-pointer ,win ,x ,y))))))
