
(in-package :wim)

(defun find-in-lists-if (finder-function &rest lists)
  (loop for list in lists
        for el = (funcall finder-function list)
        do (print el)
        when el
          do (return-from find-in-lists-if el)))
